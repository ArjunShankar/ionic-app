/**
 * Created by arjun on 16/4/17.
 */


import {Injectable} from "@angular/core";
import { Http, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class DataService{

  constructor(private http: Http,
             ) {

  }

  getData(): Observable<any[]> {

    let remoteURL: string = "https://fir-101-461f6.firebaseio.com/data.json";

    return this.http.get(remoteURL)
      .map((r: Response) => r.json())
      .catch(this.handleObservableError);
  }

  handleObservableError(error:any){
    return Observable.throw(error.json().error || 'The cats pulled the power cord....');
  }

}
