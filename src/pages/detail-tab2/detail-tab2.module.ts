import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { DetailTab2 } from './detail-tab2';

@NgModule({
  declarations: [
    DetailTab2,
  ],
  imports: [

  ],
  exports: [
    DetailTab2
  ]
})
export class DetailTab2Module {}
