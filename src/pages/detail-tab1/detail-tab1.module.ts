import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { DetailTab1 } from './detail-tab1';

@NgModule({
  declarations: [
    DetailTab1,
  ],
  imports: [
  ],
  exports: [
    DetailTab1
  ]
})
export class DetailTab1Module {}
