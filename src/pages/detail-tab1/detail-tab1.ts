import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DetailTab1 page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-detail-tab1',
  templateUrl: 'detail-tab1.html',
})
export class DetailTab1 {

  incomingData:string = ''

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.incomingData = navParams.data;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailTab1');
  }

}
