import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {DetailTab2} from "../detail-tab2/detail-tab2";
import {DetailTab1} from "../detail-tab1/detail-tab1";

@IonicPage()
@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class Details {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  details1Tab = DetailTab1;
  details2Tab = DetailTab2;
  routeData:string = '1234'

  ionViewDidLoad() {
    console.log('ionViewDidLoad Details');
  }

  goHome(){
    this.navCtrl.popToRoot({});
  }

}
