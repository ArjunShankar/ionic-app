import { NgModule } from '@angular/core';
import { Details } from './details';

@NgModule({
  declarations: [
    Details,
  ],
  imports: [
  ],
  exports: [
    Details
  ]
})
export class DetailsModule {}
