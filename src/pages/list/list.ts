import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import {DataService} from "../../shared/DataService";

import * as _ from 'lodash';

@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  selectedItem: string;
  icons: string[];
  items: Array<{title: string, note: string, icon: string}>;
  dataProvider:Array<any>;

  constructor(public navCtrl: NavController, public navParams: NavParams ,
              public dataService:DataService,
              public loadingCtrl:LoadingController) {
    // If we navigated to this page, we will have an item available as a nav param
    //this.selectedItem = navParams.get('item');
    this.selectedItem = navParams.data;

    // Let's populate this page with some filler content for funzies
    this.icons = ['flask', 'wifi', 'beer', 'football', 'basketball', 'paper-plane',
    'american-football', 'boat', 'bluetooth', 'build'];

    this.items = [];
    for (let i = 1; i < 11; i++) {
      this.items.push({
        title: 'Item ' + i,
        note: 'This is item #' + i,
        icon: this.icons[Math.floor(Math.random() * this.icons.length)]
      });
    }
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ListPage, {
      item: item
    });
  }
  ionViewDidLoad(){   //will only be called again if the page is popped off the satck
    this.getRemoteData();
  }

  getRemoteData(){
    let loader = this.loadingCtrl.create({
      content:'Loading Data.....',
      spinner:'dots'
    })
    loader.present().then(() => {
      this.dataService.getData()
        .subscribe(
          (receivedData: any) => {
            this.dataProvider =receivedData;
          },
          (errorMsg:any) =>{
            console.error("SERVICE FAILED");
          }
        )
      loader.dismiss();

      }

    )

  }

}
