import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
import { AboutUs } from './about-us';

@NgModule({
  declarations: [
    AboutUs,
  ],
  imports: [
   // IonicModule.forChild(AboutUs),
  ],
  exports: [
    AboutUs
  ]
})
export class AboutUsModule {}
