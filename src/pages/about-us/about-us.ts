import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ListPage} from "../list/list";

/**
 * Generated class for the AboutUs page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUs {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutUs');
  }

  languages:Array<string> = ['Go','Pascal','Fortran','Brainfuck','c++','java','scala','swift','TS','JS'];

  goBack(){
    this.navCtrl.pop();
  }

  listClickHandler($event,input){
    console.log(input+" clicked");
    this.navCtrl.push(ListPage,input);
  }

}
