import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AboutUs} from '../about-us/about-us'

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  gaming:string;

  items:Array<string> = ['qqq','aaaaaaa','aaaaaqw'];
  listSelectionChanged(input:any){
    console.log(input,this.gaming);
  }

  gotoAbout(){    // way to go to lower pages
    this.navCtrl.push(AboutUs)
  }

}
