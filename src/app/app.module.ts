import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {AboutUs} from "../pages/about-us/about-us";
import {Details} from "../pages/details/details";
import {DetailTab1} from "../pages/detail-tab1/detail-tab1";
import {DetailTab2} from "../pages/detail-tab2/detail-tab2";

import {DataService} from "../shared/DataService";
import { HttpModule } from '@angular/http';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    AboutUs,
    Details,
    DetailTab1,
    DetailTab2

  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    AboutUs,
    Details,
    DetailTab1,
    DetailTab2
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DataService,

    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
// all new pages --- add to entry components and declearations
